## Carlyle et al. 2020 TMT Synaptosome profiling manuscript:

**Multiplexed fractionated proteomics reveals synaptic factors associated with cognitive resilience in Alzheimer’s Disease**

---

## Contents

1. **.Rproj** and **.rmd** files contain R code required to perform these analyses
2. **Data** contains all input data files and example outputs
3. **Figures** contains figures rendered for publication

---