---
title: "5 - Create GSEA input files"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Load packages and data 

This code covers the production of GSEA input files and the list of GO terms and associated proteins.

```{r}
library(tidyverse)
library(GSA)

```

## Set up files in the right format for GSEA

```{r}
#Expression data file

TMT_data <- read.csv("Data/Output/Intermediate tables/NAsreplacedforGSEA.csv")

TMT_GSEA <- TMT_data %>%
  select(GeneName, A1:J9)

#File format for GSEA
outputPrefix = "Data/Output/GSEA/Input_files"
tmp_data_toWrite = data.frame(Name=TMT_GSEA[,1], Description=NA, TMT_GSEA[,-1], stringsAsFactors = F)

## write expression data
write.table(tmp_data_toWrite,file=paste0(outputPrefix,"_","expression.txt"), 
            sep="\t", row.names=F, quote=F)


# Phenotype decode file

sample_key <- read_csv("Data/Input/Table_S1_individual_sample_demographics.csv")

sample_key_GSEA <- sample_key %>%
   select(Code, Project.diagnosis) 



useVariable = "Project.diagnosis"

decode_out = sample_key_GSEA
labels = decode_out[match(colnames(tmp_data_toWrite)[-c(1:2)], decode_out$Code),
                    which(colnames(decode_out)==useVariable)]
labels <- labels$Project.diagnosis

classes = unique(labels)
line1 = paste(nrow(decode_out), length(classes), "1")
line2 = paste("#", paste(classes, collapse=" "))
line3 = paste(as.character(labels), collapse=" ")

## write sample information
fileConn = file(paste0(outputPrefix,"_",useVariable,"_ALL_sampleInfo.cls"))
writeLines(c(line1,line2,line3), fileConn)
close(fileConn)

```

This code produces the files that can be input to the GSEA java applet, which can be downloaded at http://software.broadinstitute.org/gsea/index.jsp .  You will find them in the Data/Output/GSEA folder.

Input_files_Project.diagnosis_reclass_ALL_sampleInfo.cls is the phenotype file
Input_files_expression.txt is the expression file

For this analysis we used the geneset available in the applet: c5.Biological Process

We used gene set permutation.

Results summary files were then saved for plotting etc in R.

##Creating gene set lists to match proteins to genesets

```{r}
library(GSA)

#Read mSig.DB list in and pull out the terms I'm interested in

REACTOME_term_list <- GSA.read.gmt("/Users/bcc27/gsea_home/output/dec03/my_analysis.Gsea.1607074510625_REACTOME_DEMvsRES/edb/gene_sets.gmt")
REACTOME_term_list_genes <- REACTOME_term_list[[1]]
names(REACTOME_term_list_genes) <- REACTOME_term_list[[2]]

genes = tibble(category=NULL, gene=NULL)
for(i in 1:length(REACTOME_term_list_genes)){
  tmp = tibble(category=names(REACTOME_term_list_genes)[i],
               gene=REACTOME_term_list_genes[[i]])
  genes = rbind(genes, tmp)
}
genes

write_csv(genes, "Data/Output/Intermediate Tables/c2.cp.reactome.v7.2.symbols.gmt.csv")
```