---
title: "9 - Comparison of GSEA terms"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

This code produces Figure 5C

```{r}
library(tidyverse)
library(UpSetR)

```


```{r}
DEMvsNOR_sig_terms <- read_csv("Data/Output/Supplementary Tables/TableS9A_DEMvsNOR_significant_GSEA_results.csv")

NORvsFRL_sig_terms <- read_csv("Data/Output/Supplementary Tables/TableS9C_GSEA_NvsFRL.csv")

DEMvsRES_sig_terms <- read_csv("Data/Output/Supplementary Tables/TableS9E_GSEA_RvDEM.csv")
  
sig_lists <- list(DEM_ADvsNOR = c(DEMvsNOR_sig_terms$NAME), NORvsFRL = c(NORvsFRL_sig_terms$NAME), 
                  DEM_ADvsRES = c(DEMvsRES_sig_terms$NAME))


Fig5C <- upset(fromList(sig_lists), order.by = "freq", nsets = 3)

pdf(file="Figures/Fig5C.pdf", width = 6, height = 6, onefile=FALSE, useDingbats = FALSE) # or other device
Fig5C
dev.off()
Fig5C



```



```{r}

all <- rbind(DEMvsNOR_sig_terms, NORvsFRL_sig_terms, DEMvsRES_sig_terms)

three <- data.frame(table(all$NAME)) %>%
  filter(Freq == 3)

two <- data.frame(table(all$NAME)) %>%
  filter(Freq == 2)

two_NORvFRL <- all %>%
  filter(NAME %in% two$Var1) %>%
  filter(comparison == "FRLoverN" | comparison == "NoverFRL") %>%
  select(NAME) %>%
  unique()

two_DEMvsRES <- all %>%
  filter(NAME %in% two$Var1) %>%
  filter(comparison == "DEMoverRES" | comparison == "RESoverDEM") %>%
  select(NAME) %>%
  unique()


```

