---
title: "7 - GSEA for NOR vs FRL"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Load packages and data 

This code covers the production of Figures 5A, 5B, Tables S9C and S9D

```{r}
library(tidyverse)
library(openxlsx)
TMT_data <- read_csv("Data/Output/Intermediate tables/NAsreplacedforGSEA.csv")

sample_key <- read_csv("Data/Input/Table_S1_individual_sample_demographics.csv")
Zscores <- read_csv("Data/Output/Intermediate tables/Protein_Z_Scores.csv")
genelist_df <- read_csv("Data/Output/Intermediate Tables/c2.cp.reactome.v7.2.symbols.gmt.csv")

```


#FRLoverN comparison - Table S9

```{r}
FRLoverN <- read_tsv("Data/Output/GSEA/gsea_report_for_FRL_1607073648602vsNOR.tsv")
NoverFRL <- read_tsv("Data/Output/GSEA/gsea_report_for_N_1607073648602vsFRL.tsv")


FRLoverN$comparison <- "FRLoverN"
NoverFRL$comparison <- "NoverFRL"


FRLoverN_GSEA_tables <- rbind(FRLoverN, NoverFRL)

#Table of significant terms
FRLoverN_sig_GSEA_results <- FRLoverN_GSEA_tables %>%
  filter(`FDR q-val` <= 0.1) %>%
  dplyr::select(NAME, SIZE, ES, NES, `NOM p-val`, `FDR q-val`, `FWER p-val`, `RANK AT MAX`, `LEADING EDGE`, comparison)

write_csv(FRLoverN_sig_GSEA_results, "Data/Output/Supplementary Tables/TableS9C_GSEA_NvsFRL.csv")

```

# Matching genes to GO terms

```{r, results="hide"}
#Character vector of significant terms - useful for pulling values out of list


FRLoverN_sig_genelist_df <- merge(FRLoverN_sig_GSEA_results, genelist_df, by.x = "NAME", by.y = "category", all.y = FALSE)

#Read in ranking table for DEMvsN comparison

FRLvsN_ranked <- read_tsv("Data/Output/GSEA/ranked_gene_list_N_versus_FRL_1607073648602.tsv")
FRLvsN_ranked <- FRLvsN_ranked %>%
  mutate(tail = ifelse(SCORE >= 0.1, "Upper", ifelse(SCORE <= -0.1, "Lower", "Middle")))%>%
  select(NAME, tail)

FRLoverN_sig_genelist_df <- merge(FRLoverN_sig_genelist_df, FRLvsN_ranked, by.x = "gene", by.y = "NAME")
FRLoverN_sig_genelist_df <- FRLoverN_sig_genelist_df %>%
  filter(comparison == "FRLoverN" & tail == "Lower" | comparison == "NoverFRL" & tail == "Upper")

write_csv(FRLoverN_sig_genelist_df, "Data/Output/Supplementary Tables/TableS9D_REACTOME_category_genelist_NvF.csv")
```

```{r}
#Make a merged table
#First have to drop some values

Zscores_gathered <- Zscores %>%
  gather(key = "Protein", value = "Zscore", -Code) %>%
  dplyr::rename(Sample = Code)


#Merge tables together and generate summary table for plotting
Zscores_to_plot <- merge(Zscores_gathered, FRLoverN_sig_genelist_df, by.x = "Protein", by.y = "gene")


FRLoverN_GO_term_summary <- Zscores_to_plot %>%
  group_by(NAME, Sample) %>%
  summarise(meanZ = mean(Zscore))

#Convert to matrix
FRLoverN_GO_to_plot <- FRLoverN_GO_term_summary %>%
  spread(NAME, meanZ) 
```

#Figure 6B

```{r}
FRLoverN_GO_to_plot <- merge(sample_key, FRLoverN_GO_to_plot, by.x = "Code", by.y = "Sample")
FRLoverN_GO_to_plot <- FRLoverN_GO_to_plot %>%
  filter(Project.diagnosis == "FRL" | Project.diagnosis == "N")

rownames(FRLoverN_GO_to_plot) = FRLoverN_GO_to_plot$Code
FRLoverN_tocluster_decode = FRLoverN_GO_to_plot[,c(1,3)]
FRLoverN_tocluster = FRLoverN_GO_to_plot[,-c(1:20)]

colnames(FRLoverN_tocluster) <- gsub("REACTOME_", "", colnames(FRLoverN_tocluster))
colnames(FRLoverN_tocluster) <- gsub("_", " ", colnames(FRLoverN_tocluster))
#Heatmap
Dx_colors <- c("DEM_AD" = "#e66101", "RES" = "#fdb863", "FRL" = "#b2abd2", "N" = "#5e3c99")

dx_col = rep("#5e3c99",nrow(FRLoverN_tocluster))
dx_col[FRLoverN_tocluster_decode$Project.diagnosis == "DEM_AD"] = "#e66101"
dx_col[FRLoverN_tocluster_decode$Project.diagnosis == "FRL"] = "#b2abd2"
dx_col[FRLoverN_tocluster_decode$Project.diagnosis == "RES"] = "#fdb863"


library(gplots)


heatmap_FRLoverN <- heatmap.2(t(as.matrix(FRLoverN_tocluster)), 
                     ColSideColors=dx_col,
                     trace="none",
                     col=colorRampPalette(c("blue","white","red"))(50)
)



```


## Most of the immune categories are derived from an enrichment of proteasomal subunits - can we collapse them into one?

```{r}
ps <- grep("PSM", FRLvsN_ranked$NAME)

proteasome <- FRLvsN_ranked[ps,1]
remove <- grep("GPSM1", proteasome$NAME)
proteasome <- proteasome[-remove,]

tmp <- FRLoverN_sig_genelist_df %>%
  group_by(NAME) %>%
  summarize(sum_prot = sum(gene %in% proteasome$NAME), n = n()) %>%
  mutate(frac_prot = sum_prot/n) 

ggplot(tmp, aes(x = frac_prot)) + geom_histogram()

#pull out terms with over 75% proteasome proteins

tmp <- tmp %>%
  dplyr::filter(frac_prot > 0.75)

FRLoverN_sig_genelist_df_no_prot <- Zscores_to_plot %>%
  filter(!(NAME %in% tmp$NAME)) %>%
  group_by(NAME, Sample) %>%
  summarise(meanZ = mean(Zscore))
  

FRLoverN_sig_genelist_df_prot <- Zscores_to_plot %>%
  filter(NAME %in% tmp$NAME) %>%
  group_by(Sample) %>%
  summarise(meanZ = mean(Zscore)) %>%
  mutate(NAME = "PROTEASOME ENRICHED TERMS")

FRLoverN_sig_genelist_df_no_prot <- rbind(FRLoverN_sig_genelist_df_no_prot, FRLoverN_sig_genelist_df_prot)
FRLoverN_sig_genelist_df_no_prot$NAME <- gsub("REACTOME_", "", FRLoverN_sig_genelist_df_no_prot$NAME)
FRLoverN_sig_genelist_df_no_prot$NAME <- gsub("_", " ", FRLoverN_sig_genelist_df_no_prot$NAME)

#Convert to matrix
FRLoverN_GO_noprot_to_plot <- FRLoverN_sig_genelist_df_no_prot %>%
  spread(NAME, meanZ) 

```


#Figure 5A - Proteasome collapsed heatmap

```{r}
FRLoverN_GO_noprot_to_plot <- merge(sample_key, FRLoverN_GO_noprot_to_plot, by.x = "Code", by.y = "Sample")
FRLoverN_GO_noprot_to_plot <- FRLoverN_GO_noprot_to_plot %>%
  filter(Project.diagnosis == "FRL" | Project.diagnosis == "N")

rownames(FRLoverN_GO_noprot_to_plot) = FRLoverN_GO_noprot_to_plot$Code
FRLoverN_noprot_tocluster_decode = FRLoverN_GO_noprot_to_plot[,c(1,3)]
FRLoverN_noprot_tocluster = FRLoverN_GO_noprot_to_plot[,-c(1:20)]


#Heatmap
Dx_colors <- c("DEM_AD" = "#e66101", "RES" = "#fdb863", "FRL" = "#b2abd2", "N" = "#5e3c99")

dx_col = rep("#5e3c99",nrow(FRLoverN_tocluster))
dx_col[FRLoverN_tocluster_decode$Project.diagnosis == "DEM_AD"] = "#e66101"
dx_col[FRLoverN_tocluster_decode$Project.diagnosis == "FRL"] = "#b2abd2"
dx_col[FRLoverN_tocluster_decode$Project.diagnosis == "RES"] = "#fdb863"

pdf(file="Figures/Fig5A.pdf", width = 5, height = 30)


heatmap_path <- heatmap.2(t(as.matrix(FRLoverN_noprot_tocluster)), 
                     ColSideColors=dx_col,
                     trace="none",
                     col=colorRampPalette(c("blue","white","red"))(50)
)


dev.off()
```




